<?php
class Account extends FrontController{
    public function Index(){
        if(isset($_SESSION['login']))
            $this->Render("PersonalAcount");
        else
            header("Location: /Account/Login");
    }
    public function Login(){
        $this->model->Valid();
        $this->Render("login");
    }
    public function Registration(){
        $this->model->RegistrationInDB();
        $this->Render("Registations");
    }
    public function LogOut(){
        $this->model->LogOut();
    }
    public function ChangePassword(){

    }
}