<?php
class Product extends FrontController{
    public function Index(){
        $productList =  $this->model->GetCollection();
        $this->Render("list",$productList);
    }
    public function Item(){
        $OunProduct = $this->model->GetElement();
        $this->Render("ItemList", $OunProduct);
    }
}