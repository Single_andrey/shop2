<?php
class Admin extends FrontController{
    public function __construct(){
        parent::__construct();
        if( $this->model->CheckUsers()!=2){header("Location: /Errors"); }
    }
    public function Index(){
           $this->Render("Admin/Admin");
    }
    public function AddItem(){
        $this->model->AddItems();
        $this->Render("Admin/AddItem");
    }
    public function DellItem()
    {
        $value = $this->model->DellItem();
        $this->Render("Admin/DellItem", $value);
    }
    public function ChangeItem()
    {
        $value = $this->model->DellItem();
        $this->Render("Admin/ChangeItem", $value);
    }
    public function AddAdmin(){}
    public function Changes(){
        $this->model->Changes();
    }
    public function Deleted()
    {
        $this->model->Deleted();
        $this->Render("Admin/DellItem");
    }
}