<?php
$pdo->exec("CREATE DATABASE IF NOT EXISTS `$databaseName`;");
$query = file_get_contents(dirname(__DIR__) . "/InstallFiles/shop.sql");
$pdo->exec("USE $databaseName");
$pdo->exec($query);
$query = file_get_contents(dirname(__DIR__) . "/InstallFiles/Roles.sql");
$pdo->exec($query);
$query = file_get_contents(dirname(__DIR__) . "/InstallFiles/Users.sql");
$pdo->exec($query);
