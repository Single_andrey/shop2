<?php
class Admins{
    public function Changes(){
        $pdo = Connect::getConnect();
        $nm = $_POST['nm'];
        $newprice = $_POST['prc'];
        $newdescription = $_POST['description'];
        $ID = $_POST['id'];

        $query = "SELECT * FROM products WHERE `Id`='{$ID}'";
        $item1 = $pdo->query($query);
        $list = $item1->fetchAll();
        $IMG = '';
        foreach($list as $item){
            $imgs = $item['image'];
            $IMG = "../pub/Media/image/".$item['image'];
        }
        if(is_uploaded_file($_FILES['imag']["tmp_name"])){
            move_uploaded_file($_FILES['imag']["tmp_name"], "../pub/Media/image/".$_FILES['imag']["name"]);
            unlink($IMG);
            $imgs = $_FILES['imag']['name'];
        }

        $query = "UPDATE  `study_andrey`.`products` SET  `description` =  '{$newdescription}', `name` ='{$nm}', `price` = '{$newprice}', `image`='{$imgs}' WHERE  `products`.`Id` ='{$ID}'";
        $pdo->query($query);
        header("Location: /Admin/ChangeItem");
    }
    public function DellItem(){
        $pdo = Connect::getConnect();
        $list = $pdo->prepare('Select * from products');
        $list->execute();
        $products = $list->fetchAll();
        return $products;
    }
    public function Deleted()
    {
        if(isset($_POST['dells'])){
            $pdo = Connect::getConnect();
            $ID = $_POST['Id'];
            $IMG = "Media/image/".$_POST['imag'];
            $query = "DELETE FROM `products` WHERE `Id`='{$ID}'";
            $pdo->query($query);
            unlink($IMG);
        }
        header("location: /Admin/DellItem");
    }
    public function AddItems(){
        $pdo = Connect::getConnect();
        if(isset($_POST['add'])){
            if($_POST['nm']==''||$_POST['desc']==''||$_POST['prc']==''){
                    echo"<tr><td></td> <td class='errs'><p>fill all fields</p></td></tr>";
            }
            else{
                $Name = $_POST['nm'];
                $description = $_POST['desc'];
                $price = $_POST['prc'];
                if($_FILES['img']["size"] > 1024*3*1024)
                {
                    echo ("<tr><td>Error</td></tr>");
                    exit;
                }
                if(is_uploaded_file($_FILES['img']["tmp_name"]))
                {
                    move_uploaded_file($_FILES['img']["tmp_name"], "../pub/Media/image/".$_FILES['img']["name"]);
                    $query = "INSERT INTO products(name, description, price, image) VALUES('{$Name}', '{$description}', '{$price}', '{$_FILES['img']['name']}')";
                    $list = $pdo->query($query);
                } else {
                    echo("Error ska!!!!!");
                }
            }
        }
    }
    public function CheckUsers(){
        $isUser = false;
        $isAdmin = false;
        $pdo = Connect::getConnect();
        if(isset($_SESSION['login'])){
            if(isset($_COOKIE["{$_SESSION['login']}"])) {
                $nameUser = $_SESSION['login'];
                $query = "SELECT * FROM Users WHERE Names = '{$nameUser}'";
                $listUser = $pdo->query($query);
                $arrUser = $listUser->fetchAll();
                $thisUser = $_SESSION['login'];
                $thisPass = $_SESSION['pass'];

                foreach ($arrUser as $item) {
                    if (isset($_COOKIE[$thisUser]) && strcmp($_COOKIE[$thisUser], $thisPass) == 0) {
                        $isUser = true;
                    }
                    if (strcmp($thisUser, "admin") == 0 && strcmp($_COOKIE[$thisUser], $item['Password'])==0) {
                        $isAdmin = true;
                    }
                }
            }
        }
        if($isAdmin) return 2;
        if($isUser) return 1;
        else return 0;
    }
}