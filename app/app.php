<?php
class app{
    public static function Run(){
        set_error_handler(function ($err_severity, $err_msg, $err_file, $err_line, array $err_context)
        {
            $f= fopen("../log/error.log",'a+');
            fwrite($f, $err_msg."\n");
            //error_log($err_msg."\n", 3,"../log/error.log");
        });
        include "../Models/Connect.php";
        include"../Layout/default.php";
        $project = new RunProject();
        $object = new $project->object();
        $name = $project->action;
        $object->$name();
    }
}